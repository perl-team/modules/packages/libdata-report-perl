libdata-report-perl (1.001-2) unstable; urgency=medium

  [ Debian Janitor ]
  * Bump debhelper from old 12 to 13.
  * Update standards version to 4.5.1, no changes needed.
  * Update standards version to 4.6.0, no changes needed.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Fri, 14 Oct 2022 12:30:46 +0100

libdata-report-perl (1.001-1) unstable; urgency=medium

  * Team upload.

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ Laurent Baillet ]
  * fix lintian file-contains-trailing-whitespace warning

  [ Debian Janitor ]
  * Bump debhelper from old 10 to 12.
  * Set debhelper-compat version in Build-Depends.

  [ gregor herrmann ]
  * Import upstream version 1.001.
  * Update years of upstream copyright.
  * Update license stanzas in debian/copyright.
  * Add debian/upstream/metadata.
  * Update build dependencies.
  * Declare compliance with Debian Policy 4.5.0.
  * Set Rules-Requires-Root: no.
  * Annotate test-only build dependencies with <!nocheck>.
  * debian/watch: use uscan version 4.

 -- gregor herrmann <gregoa@debian.org>  Fri, 21 Feb 2020 19:22:23 +0100

libdata-report-perl (0.10-3) unstable; urgency=medium

  * Team upload.

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ Niko Tyni ]
  * Update to Standards-Version 4.1.3
  * Update to debhelper compat level 10
  * Update debian/copyright to DEP-5 format

 -- Niko Tyni <ntyni@debian.org>  Sun, 14 Jan 2018 21:50:43 +0200

libdata-report-perl (0.10-2) unstable; urgency=low

  * Team upload.

  [ gregor herrmann ]
  * debian/control: Changed: Switched Vcs-Browser field to ViewSVN
    (source stanza).
  * debian/control: Added: ${misc:Depends} to Depends: field.

  [ Angel Abad ]
  * Update my email address

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ Angel Abad ]
  * Email change: Angel Abad -> angel@debian.org

  [ gregor herrmann ]
  * debian/control: update {versioned,alternative} (build) dependencies.

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ Niko Tyni ]
  * Restore explicit build dependency on libmodule-build-perl
  * Declare the package autopkgtestable
  * Move to 3.0 (quilt) dpkg source format
  * Remove duplicate build dependency on perl
  * Move to debhelper v9 tiny debian/rules

 -- Niko Tyni <ntyni@debian.org>  Fri, 05 Jun 2015 09:06:54 +0300

libdata-report-perl (0.10-1) unstable; urgency=low

  * New upstream release

 -- Angel Abad (Ikusnet SLL) <angel@grupoikusnet.com>  Wed, 20 Aug 2008 10:56:43 +0200

libdata-report-perl (0.09-1) unstable; urgency=low

  * New upstream release
  * debian/copyright: fix long line
  * debian/copyright: update copyright note
  * debian/control: Added: Vcs-Svn field (source stanza); Vcs-Browser
    field (source stanza). Changed: Maintainer set to Debian Perl Group
    <pkg-perl-maintainers@lists.alioth.debian.org> (was: Angel Abad
    (Ikusnet SLL) <angel@grupoikusnet.com>); Angel Abad (Ikusnet SLL)
    <angel@grupoikusnet.com> moved to Uploaders.
  * debian/changelog: collapse entries to unstable releases
  * debian/rules: remove README install
  * debian/control: build-depends-indep, libtest-simple-perl libtext-
    csv-perl libhtml-parser-perl
  * debina/control: depends, libtext-csv-perl libhtml-parser-perl

 -- Angel Abad (Ikusnet SLL) <angel@grupoikusnet.com>  Mon, 11 Aug 2008 21:54:00 +0200

libdata-report-perl (0.06-1) unstable; urgency=low

  * Initial Release (closes: #488975).

 -- Angel Abad (Ikusnet SLL) <angel@grupoikusnet.com>  Thu, 08 Nov 2007 11:53:20 +0100
